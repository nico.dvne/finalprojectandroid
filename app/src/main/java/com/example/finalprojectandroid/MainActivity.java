package com.example.finalprojectandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.finalprojectandroid.Service.FirstService.MyFirstService;
import com.example.finalprojectandroid.Service.MirrorService.MyMirrorServiceActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickToServiceUn(View view) {
        Intent toServiceUn = new Intent(this, MyFirstService.class);
        startActivity(toServiceUn);
    }

    public void onClickToServiceDeux(View view) {
        Intent toServiceDeux = new Intent(this, MyMirrorServiceActivity.class);
        startActivity(toServiceDeux);
    }
}