package com.example.finalprojectandroid.Service.FirstService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class PremierService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     *
     * @param intent
     * @param flags
     * @param startId
     * @return int
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("OnStartCommand", "Init");
        Runnable runnable=()->{
            while(true) {
                Log.d("Log", "Log sequentiel");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) { e.getMessage(); };
            }
        };

        Thread executeLog = new Thread(runnable);
        executeLog.start();

        return super.onStartCommand(intent, flags, startId);
    }
}
