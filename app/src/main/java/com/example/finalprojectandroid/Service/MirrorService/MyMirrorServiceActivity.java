package com.example.finalprojectandroid.Service.MirrorService;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.finalprojectandroid.R;

public class MyMirrorServiceActivity extends AppCompatActivity {

    private EditText edt_saisie;
    private TextView resultTextView;
    private Button btnReverse;

    private ServiceMiroir mirrorService;

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServiceMiroir.monBinder binder = (ServiceMiroir.monBinder)service;
            mirrorService = binder.connexion();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mirrorService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_mirror_service);

        edt_saisie = findViewById(R.id.edt_saisie_chaine);
        resultTextView = findViewById(R.id.txt_result_reverse);
        btnReverse = findViewById(R.id.btn_reverse);
        btnReverse.setEnabled(false);
    }

    public void lancerService(View view) {
        Intent intent = new Intent(this, ServiceMiroir.class);
        bindService(intent, conn, BIND_AUTO_CREATE);
        btnReverse.setEnabled(true);
    }

    public void onClickQuitter(View view) {
        this.unbindService(conn);
        finish();
    }

    public void onClickReverse(View view) {
        String revertText = mirrorService.reverse(edt_saisie.getText().toString());
        resultTextView.setText(revertText);
    }
}