package com.example.finalprojectandroid.Service.FirstService;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.finalprojectandroid.R;


public class MyFirstService extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_first_service);
    }

    public void onClickQuitter(View view) {
        finish();
    }

    public void onClickLaunchFirstService(View view) {
        Intent firstService = new Intent(this, PremierService.class);
        Log.d("tag", "nicolas");
        startService(firstService);
    }
    
}