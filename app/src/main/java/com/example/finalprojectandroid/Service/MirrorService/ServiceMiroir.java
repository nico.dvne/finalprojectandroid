package com.example.finalprojectandroid.Service.MirrorService;

import android.app.Service;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;

public class ServiceMiroir extends Service {
    public ServiceMiroir() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class monBinder extends Binder {
         public ServiceMiroir connexion() {
             return ServiceMiroir.this;
         }
    }

    private monBinder binder = new monBinder();

    @Override
    public IBinder onBind(Intent intent) {
       return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void unbindService(ServiceConnection conn) {
        super.unbindService(conn);
    }

    /**
     * @param to_reverse
     * @return
     */
    public String reverse(String to_reverse) {
        if (null == to_reverse)
            return "";

        return new StringBuffer(to_reverse).reverse().toString();
    }
}